/*---------------------
   "appStoreIntuitionLink" is used later on in javascript
   this code must be placed highest up in order to access variable
*/
var appStoreIntuitionLink = "https://apps.apple.com/us/app/intuition-brain-health-study/id1574756244"

var tracking_groups = [
   {
      url_parts: [
         'Biogen&utm_medium=Brochure', 
         'HumanGoods&utm_medium=Brochure'
      ],
      app_store_link: 'https://apps.apple.com/group-1-test'
   },
   {      
      url_parts: [
         'Rochester', 
         'Yale',
         'Pitt'
      ],
      app_store_link: 'https://apps.apple.com/group-2-test'
   }
]

/* --------------------------------
   1. Loop thru the tracking groups
   2. Check current URL and see if it contains any of the URL parts
   3. Set the app store link to the correlating group link if it matches
*/
window.addEventListener('load', function(event) {
   var current_url = window.location.href

   /*----
      Run if tracking link */
   if(current_url.includes('?utm_source')) {
      tracking_groups.forEach(function(group) {
         if(group['url_parts'].some(parts => current_url.includes(parts))) {
            appStoreIntuitionLink = group['app_store_link']
         }
         return false
      })
   }
})